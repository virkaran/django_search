from itertools import chain
from django.views.generic import ListView
from blog_post.models import Post , Lesson

class SearchView(ListView):
    model = Post
    template_name = 'view.html'
    count = 0
    paginate_by = 10

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['query'] = self.request.GET.get('q')
        return context

    def get_queryset(self):
        request = self.request
        query = request.GET.get('q', None)
        
        if query is not None:
            blog_results        = Post.objects.search(query)
            # lesson_results      = Lesson.objects.search(query) 
            # combine querysets 
            queryset_chain = chain(
                    blog_results,
                    # lesson_results,
            )        
            qs = sorted(queryset_chain, 
                        key=lambda instance: instance.pk, 
                        reverse=True)
            self.count = len(qs) 
            return qs
        return Post.objects.none()