from django.db import models
# from django.conf import settings
from django.db.models import Q
# from django.contrib.auth.models import AbstractBaseUser , BaseUserManager

# Create your models here.
class PostQuerySet(models.QuerySet):
    def search(self, query=None):
        qs = self
        if query is not None:
            or_lookup = (Q(title__icontains=query) | 
                         Q(description__icontains=query)|
                         Q(slug__icontains=query)
                        )
            qs = qs.filter(or_lookup).distinct() 
        return qs

class PostManager(models.Manager):
    def get_queryset(self):
        return PostQuerySet(self.model,using=self._db)
    
    def search(self , query=None):
        return self.get_queryset().search(query = query )

# Post models
class Post(models.Model):
    user   = models.CharField(max_length =50)
    title     = models.CharField(max_length=120)
    description   = models.TextField(null=True, blank=True)
    slug      = models.SlugField(blank=True, unique=True)
    publish_date  = models.DateTimeField(auto_now_add=False, auto_now=False, null=True, blank=True)
    timestamp   = models.DateTimeField(auto_now_add=True)
    objects    = PostManager()
    
    def __str__(self):
        return self.title
    
# courses.models
class Lesson(models.Model):
    title   = models.ForeignKey(Post ,on_delete=models.CASCADE)
    name  = models.CharField(max_length=120 , default=None)
    description   = models.TextField(null=True, blank=True)
    slug     = models.SlugField(blank=True, unique=True)
    featured   = models.BooleanField(default=False)
    publish_date  = models.DateTimeField(auto_now_add=False, auto_now=False, null=True, blank=True)

    