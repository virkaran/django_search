from django.urls import path 
from blog_post.views import SearchView

urlpatterns = [
    path('', SearchView.as_view()),
]